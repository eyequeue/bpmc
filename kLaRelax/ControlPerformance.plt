# ---------------------------------------------------------------------------------------
# controller performance plot script
#
set term aqua enhanced font "Comic Sans MS,18"
set border lw 2
set xlabel 't/h'
set ylabel 'rel. oxygen sat. / %'

set autoscale y

unset arrow
unset label


plot './pO2Mosc' title 'osc. allowed' lw 3 w l, './pO2Mno' title 'osc. not allowed' lw 3 w l




