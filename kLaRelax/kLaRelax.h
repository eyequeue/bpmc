// ---------------------------------------------------------------------------------------
//
// kLaRelax.h: abstract base class for simulation models
//
//
// ---------------------------------------------------------------------------------------

#ifndef kLaRelax_h_included
#define kLaRelax_h_included

#include "SimTool/SimModel.h"
#include "Bio/Modulation.h"

//! template for a simulation model. define differential equations of the model and
//! related functions. at this place it is possible to insert doxygen ready documentation
class kLaRelax : public SimModel {
public:

  //! default constructor
  kLaRelax();

  //! copy constructor. insert the functionality to deep copy all attributes of the model
  //! that are defined in addition (member variables that are local copies of the
  //! parameters) 
  kLaRelax(const kLaRelax& src);

  //! destructor if necessary: release memory
  virtual ~kLaRelax();

  //! clone functionality returns shared pointer to the new object
  virtual Pointer clone() const;

  //! the differential equation itself. define the model whithin this member function
  //! the actual will be transferred via the array y. it is the modellers job to evaluate
  //! the rate of change of the state and place this information in dydt. the modeller
  //! can rely on y and dydt to be of sufficient size to store as many states
  //! or rates od change as there are status names returned in the member function
  //! GetStateNames
  virtual int Equation
  (
  SIMFORMAT t,                                 //!< the time (independent variable)
  const array1d& y,                            //!< the states vector (dependent variables)
  array1d& dydt                                //!< the partials of the state vector
  );

  //! method, returning the specifications of the state variables. this method will be called by
  //! the simulation tool to reroute dynamical state quantities into their appropriate
  //! storage containers. the state initialization of the dynamic system also takes
  //! place, based on the information, given here. each differential equation to be defined must
  //! be given an associate name here.
  //! At least define a name of the state quantity. the rest is optional
  virtual SpecificationList GetStateSpecifications() const;

  //! method returning the specifications of the parameters. this method will be called by
  //! simulation tool at the initialization routine. the information given here, will be
  //! used to read the model parameters from a configuration file at runtime. a NamedQuant
  //! object will hold the parameter values intermediately. this object persist during
  //! lifetime of the object and can be accessed by all member functions.
  virtual SpecificationList GetParameterSpecifications() const;

  //! method to refresh parameters and secondary quantities. the method automatically
  //! will be called when the parameters have been set. by default it is an empty method.
  //! it may be overwritten for a derived class for performance reasons. the standard
  //! example to use this method is given, when the parameters are mapped to member vars
  //! to circumvent the query of parameters from the NamedQuant object at runtime
  virtual void RefreshParameters();

  //! define the specifications of the intermediate quantities. intermediate quantities
  //! will be computed for each simulation step. they will be stored in an an
  //! appropriate array at runtime and written to disk, named in an order determined
  //! by the specifications, defined here. therefore at least insert a name for each
  //! specification
  virtual SpecificationList GetIntermediateSpecifications() const;

  //! method, returning intermediate states. any derived class must implement
  //! this method. in case that there are no intermediates: implement a blank method.
  //! the method will be called after each integration step by the integration
  //! driver.
  //! prerequisite for the use of this method is the definition of individual names for
  //! the intermediate quantities
  virtual void GetIntermediates
  (
  SIMFORMAT      t,                              //!< the time belonging to a state
  const array1d& control,                        //!< the control quantity belonging to t
  const array1d& y,                              //!< the state itself
  const array1d& dydt,                           //!< the partial of the state (just in case)
  array1d&       intermed                        //!< the array of intermediate quantities (to be filled)
  ) const;

  //! define the specifications of scalar quantities. the output of scalar quantities will
  //! be organized according to this list.
  virtual SpecificationList GetScalarSpecifications () const;

  //! queries the values of diagnostic scalars
  virtual void GetScalars
  (
   array1d& scalars                               //!< location to store the scalar data to
   ) const;

  //! method, returning the specifications of control quantities. external quantities to be used
  //! for model control will be read e.g. from files with filenames constructed from the
  //! names stated here.
  virtual SpecificationList GetControlQuantitySpecifications() const;

  //! method, making use of external control quantities. this method will be called
  //! by the driver for the integration at the beginning each integration step. it is the 
  //! responsibillity of the user to place the control quantities in appropriate member 
  //! variables of the class.
  virtual void SetControlQuantities
  (
  SIMFORMAT      t,
  const array1d& control,                        //!< the control quatities (evaluate and copy to the appropriate member variables)
  array1d& y                                     //!< the state vector
  );

  //! routine for pre evaluations and diagnostics. this routine will be called at simulation
  //! time after the parameters have been set and after the differential equation once has 
  //! been called with the initial values.
  virtual void PreEvaluate
  (
   SIMFORMAT t,                                   //!< the initial time
   array1d& y                                     //!< the initial values
   );

  //! routine for post evaluations and diagnostics. this method will be called after the
  //! simulation has been run
  virtual void PostEvaluate
  (
   SIMFORMAT t,                                   //!< the final time
   const array1d& y                               //!< the final state
   );

protected:

  Modulation fOxy;

  // member variables (e.g. for buffering parameters) can be defined here
  SIMFORMAT rMax;
  SIMFORMAT KM;
  SIMFORMAT yXG;
  SIMFORMAT alpha;
  SIMFORMAT beta;
  SIMFORMAT KH;
  SIMFORMAT pAtm;
  SIMFORMAT tau;

  SIMFORMAT pO2set;
  SIMFORMAT Kresp;
  SIMFORMAT TI;
  SIMFORMAT pO2;

  SIMFORMAT fStirr;


};


#endif // kLaRelax_h_included


