# ---------------------------------------------------------------------------------------
# response analysis plot script
#
set term aqua enhanced font "Comic Sans MS,18"
set border lw 2
set xlabel 't/h'
set ylabel 'rel. oxygen sat. / %'

set yrange [0.0:17.5]


set arrow 10 from 0.000, 17.0 to 0.100, 17.0 nohead lw 2
set arrow 20 from 0.007,  0.0 to 0.007, 17.5 nohead lw 2
set arrow 30 from 0.054,  0.0 to 0.054, 17.5 nohead lw 2

set label 40 '0.007 h' at 0.01,0.50
set label 50 '0.054 h' at 0.056,0.50
set label 60 'T_L^* = 0.07 h' at 0.06, 12
set label 70 'K_P=0.34 %/rpm' at 0.06, 13
set label 80 'T_R = 0.054 h - 0.007 h = 0.047 h' at 0.06,11

plot './pO2M.sim' title '' lw 3 w l, 357*x-2.30 title '' lw 2

# unset arrow
# unset label
# plot './pO2M.sim' w l
# set autoscale y
# plot './pO2M.sim' w l
# plot './pO2M.sim' w l
# plot './pO2Mno' w l, './pO2Mosc' w l
# plot './pO2Mosc' title 'osc. allowed' lw 3 w l
# replot './pO2Mno' title 'osc. not allowed' lw 3 w l
# 
